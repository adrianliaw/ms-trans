0
00:00:05,049 --> 00:00:09,879
統計學習理論裡最重要的概念就是奧卡姆剃刀原則。

1
00:00:09,879 --> 00:00:16,820
奧卡姆剃刀的概念就是：最棒的模型都是簡單、而適用於那資料上的模型。

2
00:00:16,820 --> 00:00:23,279
它的名字來自於一名聖方濟各會修士和哲學家，他說

3
00:00:23,279 --> 00:00:26,710
在許多能作出同樣準確的預言的假說之中，我們應該選擇其中使用最少假設的。

4
00:00:26,710 --> 00:00:30,880
我很確定他在說這個理論的時候並沒有想著統計學習理論，

5
00:00:30,880 --> 00:00:37,760
但是這就是它的由來。好的，我們先從基本的一維迴歸模型開始。

6
00:00:37,760 --> 00:00:45,030
現在畫面上這個模型不好，它非常的過適。

7
00:00:45,030 --> 00:00:50,210
它沒有辦法一般化到新的資料上，它就是沒辦法預測的好。

8
00:00:50,210 --> 00:00:57,070
今天假如我有一個方法可以衡量模型複雜度，

9
00:00:57,070 --> 00:01:04,589
而越複雜的模型越容易「過適」，越簡單的模型越容易「乏適」。

10
00:01:04,589 --> 00:01:11,400
這個圖表對於暸解學習理論非常重要。如果我把訓練錯誤作圖出來，

11
00:01:11,400 --> 00:01:18,760
也就是這個曲線，那麼隨著模型變得越來越複雜，

12
00:01:18,760 --> 00:01:25,360
訓練錯誤變得越小，反正可以越來越過適，

13
00:01:25,360 --> 00:01:33,250
但同時，如果我這樣做，測試錯誤就會變得越來越大。相反的，如果乏適，

14
00:01:33,250 --> 00:01:38,490
那模型就在訓練和測試上都做得不好。我們的目標就是中間這個點。

15
00:01:38,490 --> 00:01:45,830
這個概念在分類、迴歸等等都一樣成立。

16
00:01:45,830 --> 00:01:51,240
總之概念就是，最好的模型是簡單而合適於資料上的的模型。

17
00:01:51,240 --> 00:01:56,730
所以我們需要的是精準度和精簡度之間的平衡。

18
00:01:56,730 --> 00:02:04,560
不過，奧卡姆可能不知道最佳化，但我猜如果他現在活著，

19
00:02:04,560 --> 00:02:11,590
這也會很適合他。所以最常見的機器學習方法，它們選擇它們的 f 函數，

20
00:02:11,590 --> 00:02:20,630
來最小化訓練錯誤和模型複雜度，為了阻止「維數災難」，

21
00:02:20,630 --> 00:02:26,560
所謂的「維數災難」就是，當我們擁有非常多的特徵而沒有那麼多的資料，

22
00:02:26,560 --> 00:02:32,450
很容易出現過適的現象。資料隨著特徵數量變多，

23
00:02:32,450 --> 00:02:39,950
會需要指數型速度的增加，才不會有過適的問題。

24
00:02:39,950 --> 00:02:46,090
所以我們要選一個不複雜、低訓練錯誤的模型。

25
00:02:46,090 --> 00:02:53,160
而這就符合了奧卡姆剃刀原則。

26
00:02:53,160 --> 00:02:59,550
複雜度可以用幾種方法來衡量，通常被叫做「正規化」。

27
00:02:59,550 --> 00:03:05,240
那麼這就是機器學習最重要的基礎，它就是要創造一個

28
00:03:05,240 --> 00:03:10,170
同時能最小化損失，又能維持模型簡單的函數。各位，這就是我們所有的出發點，

29
00:03:10,170 --> 00:03:13,700
我們在這門課中將會用各種不同的方法來做到這件事。

30
00:03:13,700 --> 00:03:17,540
不同的機器學習方法有不同的損失函數和正規化方法。

31
00:03:17,540 --> 00:03:21,780
所以，隨著課程的前進，你將會看到越來越多這些機器學習方法的內部，

32
00:03:21,780 --> 00:03:23,130
因為我會告訴你所有這些方法。

