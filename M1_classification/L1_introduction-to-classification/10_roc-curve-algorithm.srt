0
00:00:05,390 --> 00:00:10,660
來講一下另外一個產生 ROC 曲線的做法。

1
00:00:10,660 --> 00:00:19,119
ROC 可以由兩種方法產生：第一個就是我剛給你看過的，也就是單一、真實的分類器。

2
00:00:19,119 --> 00:00:25,740
在那種情況下，ROC 曲線是用來評價分類器的。

3
00:00:25,740 --> 00:00:32,850
還有另外一種產生 ROC 曲線的方法，你可以用一個演算法，然後滑動不平衡參數，

4
00:00:32,850 --> 00:00:40,219
滑過它的整個廣度，並且記錄出一個 ROC 曲線，來評價這個演算法。

5
00:00:40,219 --> 00:00:44,149
所以其中一個是單一分類器的屬性，另一個是整個演算法的屬性。

6
00:00:44,149 --> 00:00:49,070
那我再帶過一次，我會重複一次怎麼做到它。假設我們現在有一個分類器，

7
00:00:49,070 --> 00:00:55,910
也就是我們的 f 函數，而它沿著這個方向來增加。

8
00:00:55,910 --> 00:01:01,289
而我們在這邊的任何地方放置決策邊界，那麼就從最上面開始，

9
00:01:01,289 --> 00:01:05,580
從最上面滑到最下面，每一次移動它，

10
00:01:05,580 --> 00:01:09,720
就記錄真陽性比率和假陽性比率。然後就把所有的 CPR 和 FPR 畫成散佈圖，

11
00:01:09,720 --> 00:01:15,020
而這就成為了 ROC 曲線，那就先這麼做。

12
00:01:15,020 --> 00:01:19,950
所以像這樣去移動它，當我們在做這件事的同時，要記錄真陽性比率和假陽性比率。

13
00:01:19,950 --> 00:01:25,780
它這樣就描繪出了這樣的曲線。所以這是創造 ROC 曲線的第一個方法，

14
00:01:25,780 --> 00:01:31,479
而它是為了單一一個分類模型。那現在來講，如何評價一個演算法。

15
00:01:31,479 --> 00:01:36,520
你還記得不平衡學習那一節裡面的 C 參數嗎？

16
00:01:36,520 --> 00:01:42,409
它就是能夠用來給予陽性和陰性不同權重的東西。

17
00:01:42,409 --> 00:01:49,799
所以這邊我要走過各個 C 參數的值，每次調整 C 的時候就套一次學習模型。

18
00:01:49,799 --> 00:01:54,420
而我們先從很小的 C 開始。所以分類器根本就不管陽性，

19
00:01:54,420 --> 00:01:58,880
它只在乎把陰性正確歸類。你猜猜發生什麼事了？

20
00:01:58,880 --> 00:02:04,619
它全都答對了，因為就只是把所有的東西都歸類為陰性。

21
00:02:04,619 --> 00:02:11,700
接著調整 C，就得到了這個。然後再次調整 C，得到這條決策邊界，然後這個，

22
00:02:11,700 --> 00:02:15,950
然後這個。而當你滑過它的同時，你每次都得到不同的模型，

23
00:02:15,950 --> 00:02:21,030
但你每次也得到一個真陽性比率和假陽性比率。

24
00:02:21,030 --> 00:02:28,360
所以你可以把這些值畫成 ROC 曲線。但這個 ROC 曲線評價了整個演算法，而不是一個分類器。

25
00:02:28,360 --> 00:02:33,060
那麼，使用其中一個方法而不是另一個的好處是什麼呢？

26
00:02:33,060 --> 00:02:37,810
其實你不能這樣講，其中一個是用來評價一個函數，

27
00:02:37,810 --> 00:02:42,849
而另一個用來評價整個演算法的品質。

28
00:02:42,849 --> 00:02:48,879
通常當一個演算法為了某個決策重點而做優化，

29
00:02:48,879 --> 00:02:55,300
它會做得比為了其他特點而做優化的演算法還要好。

30
00:02:55,300 --> 00:03:01,750
所以通常你會預期整個演算法的 ROC 曲線，也就是在曲線上的每一個點都被優化過的，

31
00:03:01,750 --> 00:03:07,860
你會預期它做的比單一的分類器還要好，

32
00:03:07,860 --> 00:03:11,489
雖然說有時候就是有一些驚喜和怪事的發生。但無論如何，概念就是這樣。

33
00:03:11,489 --> 00:03:16,980
如果你用了演算法，然後來回調整類別權重參數，

34
00:03:16,980 --> 00:03:25,640
你基本上就是在最佳化曲線上的每一個點。反之，如果你是用一個固定的分類器，

35
00:03:25,640 --> 00:03:30,920
你可能就只是為了曲線上的其中一個點做最佳化，或是完全為了其他的東西，

36
00:03:30,920 --> 00:03:35,650
你就不會預期 ROC 曲線很好。好的，到結尾了，

37
00:03:35,650 --> 00:03:37,510
這些就是產生 ROC 曲線的方法。

