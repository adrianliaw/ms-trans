0
00:00:05,069 --> 00:00:11,019
我們來談談異常值，尤其是高影響力點以及槓桿作用。

1
00:00:11,019 --> 00:00:19,050
一個高影響力點就是當你忽略它時會可觀地改變估計量的點。

2
00:00:19,050 --> 00:00:25,650
而這能製造機器學習很難的課題。所以這是我們的模型

3
00:00:25,650 --> 00:00:31,269
應該要長的樣子，所有東西都很棒都在那條線附近，但這傢伙出現在這，

4
00:00:31,269 --> 00:00:36,000
而如果你在上面這裡有個點，你其實會把正條線往上移動，

5
00:00:36,000 --> 00:00:40,980
因為你要最小化平方差總和，結果這條線

6
00:00:40,980 --> 00:00:45,250
最後長這樣，所以現在它不再適合於所有這些資料，因為

7
00:00:45,250 --> 00:00:49,460
它試圖容納那個點。而你也將看到這些不好的點在

8
00:00:49,460 --> 00:00:55,340
殘差圖上。記住殘差圖是每個點和模型的距離的直方圖。

9
00:00:55,340 --> 00:01:00,149
所以，這裡會有隆起處，大部分的點都在這裡，然後

10
00:01:00,149 --> 00:01:05,820
你會看到這個點在遠處，就像我們看到的捷豹。現在，如果

11
00:01:05,820 --> 00:01:16,000
移除那個點，那麼 f 的值就會完全不同，如果你把那個點拿掉，

12
00:01:16,000 --> 00:01:21,820
f 現在是完全不同的，它現在一路往下到這裡。所以，你必須要小心

13
00:01:21,820 --> 00:01:25,430
注意高影響力點，因為他們真的能把你的模型拉偏離總體真值。

14
00:01:25,430 --> 00:01:31,750
評估每一個離群值以確任它的標籤和特徵是否為正確的

15
00:01:31,750 --> 00:01:36,810
是很有用的。無論你是要修正那個點，或者是你要把拿出來。

16
00:01:36,810 --> 00:01:44,540
那麼槓桿作用則是比較不同的東西。讓我先給你定義，直覺是這樣的：

17
00:01:44,540 --> 00:01:54,689
你看到這裡這個點了嗎？如果你移動它，不會發生太多事情。

18
00:01:54,689 --> 00:01:59,630
即使你移動它多一點，也沒什麼事情發生。這一個點

19
00:01:59,630 --> 00:02:07,659
則是另外一回事了，因為這一點的 x 值是遠離其他的，

20
00:02:07,659 --> 00:02:14,410
在這一點上的變化將會對 f 的值產生較大的變化。

21
00:02:14,410 --> 00:02:18,919
即使我移動這兩個點：這裡一個、另外一個在那裡，即使我移動相同的量，

22
00:02:18,919 --> 00:02:24,480
函數 f 的值將會因為這個點移動的比另外一個點多。

23
00:02:24,480 --> 00:02:33,299
這就是所謂的槓桿作用。更確切地說，如果移動 x，則 f(x) 相對地移動。

24
00:02:33,299 --> 00:02:40,130
這個相對性的常數被稱為，在 i 點上的槓桿量。

25
00:02:40,130 --> 00:02:45,389
槓桿量取決於 x，但它和 y 沒有關係。它只是取決於 x 和 x 的平均

26
00:02:45,389 --> 00:02:52,180
之間的距離。所以這裡這個點具有高槓桿量，因為

27
00:02:52,180 --> 00:02:57,859
它離其他點很遠。它不是一個高影響力點對吧？

28
00:02:57,859 --> 00:03:02,019
我們若把它拿走，什麼都不會發生。它不會在殘差圖上很突兀的

29
00:03:02,019 --> 00:03:09,479
突出來。更確切地說，高影響力點不一定具有高槓桿量，

30
00:03:09,479 --> 00:03:17,220
而高槓桿量的點不一定是高影響力的。而這裡這個點如我提到的，

31
00:03:17,220 --> 00:03:21,979
是非常高影響力的，但它並不具有高槓桿量，如果失去那個點，

32
00:03:21,979 --> 00:03:26,840
迴歸會改變，但它的槓桿量低，因為如果你移動它一點點，迴歸線不會

33
00:03:26,840 --> 00:03:32,829
發生很大的變化。這另外一個點則具有高槓桿量，但它影響力不高，

34
00:03:32,829 --> 00:03:40,220
因為如果你刪除它不會有什麼改變，因為它就在那條線上。

35
00:03:40,220 --> 00:03:47,410
x 的槓桿量衡量了在 f(x) 上對 y 的影響。結束之前，我們就多談一點點

36
00:03:47,410 --> 00:03:53,750
更多關於異常值的審查。你會想要評估資料的正確性，

37
00:03:53,750 --> 00:03:58,780
以及模型的完整性和正確性。所以，當我發現一個異常值，我通常會

38
00:03:58,780 --> 00:04:02,699
挖進資料裡，弄清楚那個點是否真的正確。有時候，你進去之後

39
00:04:02,699 --> 00:04:07,530
你會發現輸入資料的那個人，在那個點上多打了一個 0，

40
00:04:07,530 --> 00:04:13,419
所以把 10 打成 100，然後你就校正它或刪除它。而有時候

41
00:04:13,419 --> 00:04:17,229
你就是沒辦法修好它，有時候資料是正確的，只是模型並沒有

42
00:04:17,228 --> 00:04:22,970
納入你知道的一些重要因素，那些你真的需要的東西。而這就是當建模

43
00:04:22,970 --> 00:04:26,070
變得更是一種藝術而非科學的時候。你必須弄清楚如何

44
00:04:26,070 --> 00:04:28,960
以真正有道理的方法來描述這個世界。

