0
00:00:04,950 --> 00:00:10,010
讓我們來談談如何評價迴歸模型。我們必須找出一個辦法來

1
00:00:10,010 --> 00:00:15,730
評估真實值和預測值 f(x) 的相近度。因此，這是我稍早前提出的

2
00:00:15,730 --> 00:00:23,540
y - f，而我們不喜歡，因為我們可以對等的提出這一個。

3
00:00:23,540 --> 00:00:30,860
所以我們要使用一個能懲罰任一方向的錯誤的東西。因此，我們可以使用距離，

4
00:00:30,860 --> 00:00:35,620
這是預測和真值之間的距離，我們可以

5
00:00:35,620 --> 00:00:39,500
評估平均絕對誤差，也就是這些東西的總和，這是其中一種方法。

6
00:00:39,500 --> 00:00:43,500
但你也可以把它們全部做平方，並得到最小平方差或

7
00:00:43,500 --> 00:00:49,060
方差總和或均方差。而你可以把它看作

8
00:00:49,060 --> 00:00:54,130
擷取兩個方向的錯誤，就像我們先前用的絕對值。同樣的

9
00:00:54,130 --> 00:00:59,470
以 y 和 f 之間任一方向的距離做懲罰，然後均方根誤差就只是

10
00:00:59,470 --> 00:01:04,899
平方誤差和的平方根。現在話題轉移到殘差，

11
00:01:04,899 --> 00:01:12,439
殘差或誤差就是預測值和真值之間的差異。

12
00:01:12,439 --> 00:01:17,950
顯然的我們希望全部都接近 0，而且我們也不想在這些殘差中

13
00:01:17,950 --> 00:01:22,380
看到任何結構或模式，因為那就會代表著我們需要

14
00:01:22,380 --> 00:01:26,819
做更多沒做到的模型。我們來多討論這個一點。

15
00:01:26,819 --> 00:01:30,780
我們希望殘差越接近 0 越好，我們不希望有任何

16
00:01:30,780 --> 00:01:36,219
明顯的模式。那接著我要做的是，我要畫出所有殘差的直方圖

17
00:01:36,219 --> 00:01:44,229
y - f(xi)，而直方圖較高的地方就是大多數殘差的值。

18
00:01:44,229 --> 00:01:49,369
而理想情況下，他們大多都是接近 0。

19
00:01:49,369 --> 00:01:56,119
所以這樣子是不錯的。大多數的殘差都靠近 0，而這些殘差

20
00:01:56,119 --> 00:02:00,859
沒有什麼模式。你如果看到這種東西就可以笑了，因為你就知道

21
00:02:00,859 --> 00:02:06,789
你做對了些事情。反著說，這樣子是不好的，這可能代表我們錯失了些東西。

22
00:02:06,789 --> 00:02:12,470
那麼，這種圖要怎麼解釋？

23
00:02:12,470 --> 00:02:17,700
也許我們正在用一條線之類的東西來建模在資料上，但事實上

24
00:02:17,700 --> 00:02:23,319
資料是按照三條線之類的，也就是為什麼會有三處隆起。也許我們就是錯失了另外兩條線。

25
00:02:23,319 --> 00:02:28,670
我也不知道，我不能只從這張圖就看出確切的問題。

26
00:02:28,670 --> 00:02:32,879
為了弄清楚是什麼導致這個，我們要搞清楚在這一塊裡的每個東西

27
00:02:32,879 --> 00:02:36,260
的共同因素是什麼，然後弄清楚在那一塊裡大家的共同處，

28
00:02:36,260 --> 00:02:41,110
然後我們必須把這些額外因素包含在迴歸裡，

29
00:02:41,110 --> 00:02:47,239
因此我們可以以任何存在的訊息建模，試圖除掉這些凹凸不平。

30
00:02:47,239 --> 00:02:52,799
這樣也很差，也許我們就是用了錯誤的模型，有很多的點是

31
00:02:52,799 --> 00:03:01,829
我們沒有預測好的。我們有一堆點是離 f(x) 一點都不近的。

32
00:03:01,829 --> 00:03:08,480
這是汽車價格資料集的特徵列表，我們有每一台車的

33
00:03:08,480 --> 00:03:15,109
所有這些因素，然後我們要是著預測價格。而我們要用一些特徵

34
00:03:15,109 --> 00:03:20,549
來做個迴歸並試著預測價格。你如果好奇的話，

35
00:03:20,549 --> 00:03:28,230
其實我們用了燃料類型這個特徵，也就是汽油或柴油；進氣類型，

36
00:03:28,230 --> 00:03:37,719
也就是渦輪增壓與否，以及汽車淨重等。我們用這些東西來預測汽車的價格。

37
00:03:37,719 --> 00:03:45,620
而這是用 R 產生出來的一張圖，還滿有趣的，

38
00:03:45,620 --> 00:03:52,989
在這邊，這個軸是預估的價格，所以越昂貴的汽車越靠近這裡。

39
00:03:52,989 --> 00:04:00,170
現在，我要你注意這裡的這些點。每個點都是一個汽車，

40
00:04:00,170 --> 00:04:04,999
你可以看到，對於便宜的車的預測是很不錯的。便宜的車子的

41
00:04:04,999 --> 00:04:09,450
殘差都接近 0。但之後到的較昂貴的汽車時，

42
00:04:09,450 --> 00:04:20,049
預測越來越糟，我們的預測離真值越來越遠。那為什麼會這樣呢？

43
00:04:20,048 --> 00:04:24,770
我們的確有大量的便宜汽車，但迴歸模型會試著去多適應便宜汽車一點，

44
00:04:24,770 --> 00:04:29,420
只是因為它們比較多。但我和 Steve 講這個的時候，

45
00:04:29,420 --> 00:04:34,700
他則認為這是因為較昂貴的車較多樣化，所以就是沒辦法

46
00:04:34,700 --> 00:04:41,840
對昂貴汽車預測得很好。這個是怎樣？我們根本就完全沒有

47
00:04:41,840 --> 00:04:49,040
預測到那個的價格。結果這輛車是一台昂貴的 12 汽缸

48
00:04:49,040 --> 00:04:54,650
的捷豹且有一些不尋常的特性。事實上它是雙座的，還一個巨大的車前罩和

49
00:04:54,650 --> 00:05:00,670
巨大的引擎。其實這張圖表有以汽缸數量上色。

50
00:05:00,670 --> 00:05:05,920
所以看得出來，我們對於汽缸較少的預測，做得比汽缸較多的好。

51
00:05:05,920 --> 00:05:11,230
我認為這挺有趣的，所以我們可能要考慮一下把

52
00:05:11,230 --> 00:05:18,350
汽缸數量加到模型裡。然後這是一個殘差圖，也就一樣又是一個

53
00:05:18,350 --> 00:05:24,500
殘差的直方圖，而幸運的是，它很棒的集中在 0 左右，

54
00:05:24,500 --> 00:05:27,840
然後，捷豹則在這裡。

