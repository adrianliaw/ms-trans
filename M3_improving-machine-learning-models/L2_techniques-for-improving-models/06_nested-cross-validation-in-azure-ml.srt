0
00:00:05,230 --> 00:00:14,750
大家好，我們已經看過了糖尿病分類模型的交叉驗證，

1
00:00:14,750 --> 00:00:24,439
而如果你還記得，我們在以前的示範中，走過一系列的

2
00:00:24,439 --> 00:00:32,689
特徵集的改變，利用排列式特徵重要性，我們削減特徵。所以一個

3
00:00:32,689 --> 00:00:39,750
巢狀交叉驗證的範例就是 Cynthia 剛討論過的，就是對這些從不同的資料集

4
00:00:39,750 --> 00:00:47,340
所產生出來的不同的模型做交叉驗證，有不同的特徵數量，

5
00:00:47,340 --> 00:00:55,760
而那就是我們最初評估的結果，就是說移除多餘的特徵真的沒有

6
00:00:55,760 --> 00:01:01,420
顯著的影響準確度或任何指標，透過使用這些巢狀的方法，

7
00:01:01,420 --> 00:01:07,390
交叉驗證會讓我們更方便的做這件事。所以我們要比較這些，在我的

8
00:01:07,390 --> 00:01:15,610
螢幕上，有我們一直在做的實驗。記得我們之前在這裡做什麼，

9
00:01:15,610 --> 00:01:23,470
我們削減特徵，我們有一個模型，在這裡被創造和評價，

10
00:01:23,470 --> 00:01:30,570
包含所有的特徵，我們用了這個排列式特徵重要性模組，來決定

11
00:01:30,570 --> 00:01:37,750
我們要削減哪些特徵，然後我們又看了一次要削減哪些特徵，然後

12
00:01:37,750 --> 00:01:45,230
又做了一次，事實上我們做了四次。你可以看到，我添加了一個

13
00:01:45,230 --> 00:01:55,780
交叉驗證模組在這裡、這裡、這裡和這裡。現在我不一定要

14
00:01:55,780 --> 00:02:01,450
查看所有可能的組合，但我們只要看看起點

15
00:02:01,450 --> 00:02:06,920
以及終點，來看看我們是否已經不經意的在這個削減的程序中

16
00:02:06,920 --> 00:02:15,170
造成了一些傷害，所以我就執行這個實驗。那實驗執行完了，現在我要

17
00:02:15,170 --> 00:02:21,730
在這裡用一個技巧，讓我們可以把結果並排看。而根據你的

18
00:02:21,730 --> 00:02:28,950
瀏覽器，這不一定會比較好或比較差。我就打開另一個視窗，

19
00:02:28,950 --> 00:02:40,120
看和我們剛跑過的完全相同的實驗。並排擺放而不是用分頁，

20
00:02:40,120 --> 00:02:49,499
而在這裡我要為第一個模型的交叉驗證結果做視覺化，

21
00:02:49,499 --> 00:02:57,349
也就是我們沒有削減特徵的地方，然後我們一路滾動到這裡，

22
00:02:57,349 --> 00:03:03,939
我們要找到最後一個模型的交叉驗證，就是我們削減了不少特徵

23
00:03:03,939 --> 00:03:13,319
的地方，我們已經講過怎麼看這個。那麼就來看看

24
00:03:13,319 --> 00:03:19,889
這三欄，準確度、精確率、召回率，我們就並排比對他們，

25
00:03:19,889 --> 00:03:27,010
其實我要直接切入下面這裡，這些平均值，

26
00:03:27,010 --> 00:03:36,029
我要滾動過來，讓我們可以看一下精確度、精準率、召回率、F1 和 AUC

27
00:03:36,029 --> 00:03:45,430
在這邊做一樣的事，基本上看到的是它們都非常相似，0.64、0.64，

28
00:03:45,430 --> 00:03:56,810
0.64、0.64，0.51、0.51 等，你可以看到標準差是相當小的，

29
00:03:56,810 --> 00:04:03,370
如我們喜歡的，有些數字的差別是在很小的位數。所以透過巢狀化

30
00:04:03,370 --> 00:04:09,129
這兩個交叉驗證，我們可以看到我們已經從我們的模型中消除了不少特徵，

31
00:04:09,129 --> 00:04:16,380
而實際上我們完全沒有影響性能，所以我們能的有一個

32
00:04:16,380 --> 00:04:22,980
滿高的自信能說這是正確的事。所以記得

33
00:04:22,980 --> 00:04:28,980
巢狀交叉驗證是相當籠統的，你可以用它來比較不同的模型、

34
00:04:28,980 --> 00:04:39,290
在這情況下是不同的模型參數，不同的特徵子集，

35
00:04:39,290 --> 00:04:43,390
有較多一點的信心度，有時候是多非常多的信心度，就是說那不只是

36
00:04:43,390 --> 00:04:51,090
從單一一個樣本的統計上的巧合，你是在比較兩個模型的結果。

