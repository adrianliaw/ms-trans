0
00:00:05,100 --> 00:00:10,920
本節的概念是正規化，而他就是很多機器學習方法

1
00:00:10,920 --> 00:00:17,760
可以一般化的原因。所以讓我們談談它吧。記得統計學習理論裡的，

2
00:00:17,760 --> 00:00:22,140
為了保持我們在測試集上的誤差是小的，我們必須要使模型

3
00:00:22,140 --> 00:00:27,710
在訓練集上表現的準確，但也需要保持它的簡單。如果你沒有做正規化，

4
00:00:27,710 --> 00:00:31,490
你沒有辦法控制模型的簡單性。那麼你就只是最小化

5
00:00:31,490 --> 00:00:35,820
訓練誤差，然後你就會這樣過適。所以問題點在於

6
00:00:35,820 --> 00:00:40,339
我們允許讓模型過於複雜。如果我們限制模型的

7
00:00:40,339 --> 00:00:46,819
複雜度，我們可以做的更好。如果你使用正規化，它事實上限制了

8
00:00:46,819 --> 00:00:51,579
你模型的複雜度，這樣當你把正規化和訓練誤差一起做最小化，

9
00:00:51,579 --> 00:00:55,769
你就是最小化簡單一點的模型的誤差，那麼你就在好區域。

10
00:00:55,769 --> 00:01:02,109
那你就可以一般化，你的測試誤差實際上會是很好的。這裡的簡單度

11
00:01:02,109 --> 00:01:07,810
是由正則化項，和常數 C 做測量的，這常數是非常非常

12
00:01:07,810 --> 00:01:13,140
重要的，因為它決定了精確度和簡單度之間的取捨。

13
00:01:13,140 --> 00:01:21,600
現在我們從這裡開始，正規化的損失函數，而至於 f 模型，

14
00:01:21,600 --> 00:01:27,369
我們要選擇一個線性模型，只是因為它自然且容易。而係數

15
00:01:27,369 --> 00:01:34,140
被稱為 β。為了正規化，為了簡單起見，我會要求

16
00:01:34,140 --> 00:01:43,630
係數要小。簡單的模型就是擁有比較小的係數的模型。

17
00:01:43,630 --> 00:01:49,229
所以我告訴你，有兩個選項，或說兩個正規化項，促使那些係數

18
00:01:49,229 --> 00:01:55,369
保持較小。首先，你可以把係數全部做平方，然後把它們全加起來，

19
00:01:55,369 --> 00:02:00,439
這樣的話，當你最小化這個正則化項，它會促使所有這些係數

20
00:02:00,439 --> 00:02:05,539
都保持很小。那現在係數的平方和，被稱為 l2 範數，

21
00:02:05,539 --> 00:02:11,480
這就是所謂的 l2 正規化。而這是另一種做法，我們把

22
00:02:11,480 --> 00:02:18,880
係數的絕對值全加起來，而這就是所謂的 l1 範數以及 l1 正規化。

23
00:02:18,880 --> 00:02:25,140
現在來看看這些東西在幹嘛，我們嘗試做個一維的例子。

24
00:02:25,140 --> 00:02:30,920
那麼在一維中會發生什麼事？我們只有一個係數，叫做 β1。

25
00:02:30,920 --> 00:02:36,660
所以正規化的時候，我們要求 β1 的平方要小或者是

26
00:02:36,660 --> 00:02:41,530
它的絕對值要小，基本上是一樣的：讓 β1 小。

27
00:02:41,530 --> 00:02:48,420
現在假如我們正在做一個老式單純的迴歸，最小平方法之類的。

28
00:02:48,420 --> 00:02:58,250
那正規化會做什麼呢？那麼這是我們的正規化，它的作用是它減少了

29
00:02:58,250 --> 00:03:04,530
該直線的斜率，所以也許正規化的版本看起來像這樣，

30
00:03:04,530 --> 00:03:09,840
而不是上一個這個。它看起來有點微不足道，但正規化真的在做的

31
00:03:09,840 --> 00:03:14,610
是它幫助那條線保持較平坦，意思就是說，

32
00:03:14,610 --> 00:03:18,760
不要太關注資料的變化，我想試著忽略它。

33
00:03:18,760 --> 00:03:24,730
你讓我多平我就讓它多平。這可能對這條線來說不那麼明顯，

34
00:03:24,730 --> 00:03:31,180
但對於高維多項式呢？那我們繼續先保持一維，

35
00:03:31,180 --> 00:03:38,300
但我們會讓其他變數都是 x 的單項式。所以讓我設定好，

36
00:03:38,300 --> 00:03:43,050
我們只有一個變數 x，但我們已經平方它也立方它，

37
00:03:43,050 --> 00:03:48,230
還有很多彎曲的多項式，而它們可以各自具有不同的係數，所以

38
00:03:48,230 --> 00:03:52,980
它仍然是一個線性模型，它是一堆非線性項的線性模型，

39
00:03:52,980 --> 00:03:58,840
即是這些所有的單項式。但既然你已經預先計算所有非線性的東西，

40
00:03:58,840 --> 00:04:03,090
剩下的建模其實是線性的。我有點像是在作弊，我說這是一個線性模型，

41
00:04:03,090 --> 00:04:08,970
但它其實超級非線性，這是項數為 p 的多項式。電腦不知道

42
00:04:08,970 --> 00:04:13,350
我把那些東西做平方和立方等等的，所以對電腦而言，

43
00:04:13,350 --> 00:04:18,840
這就是一個線性模型。那讓我們先不用正規化，來適應它，它看起來應該

44
00:04:18,839 --> 00:04:22,600
向這樣的，有點像這樣，雖然我畫這個只是為了誠實一點，

45
00:04:22,600 --> 00:04:28,530
但它看起來有點像那樣。現在如果我添加正規化，它會想要

46
00:04:28,530 --> 00:04:34,560
讓所有的 β 比較小，所以它會減少很多的彎曲度，

47
00:04:34,560 --> 00:04:39,740
因為正規化不會讓它這麼在乎資料。所以正規化

48
00:04:39,740 --> 00:04:43,820
它會試著同時盡量的擺脫多項式，並且仍然對資料有

49
00:04:43,820 --> 00:04:50,100
在乎一些，有點像這樣。而對於這種模型，我會說

50
00:04:50,100 --> 00:04:56,720
更可能的會比前一個能夠一般化，也就是嚴重的過適。

51
00:04:56,720 --> 00:05:01,980
現在你知道正規化在做什麼了，讓我告訴你

52
00:05:01,980 --> 00:05:13,220
l1 和 l2 正規化之間的差別。l2 正規化往往直觀地讓所有的係數

53
00:05:13,220 --> 00:05:19,820
都保持的算小，而 l1 正規化則有點不同，這個對於

54
00:05:19,820 --> 00:05:27,840
產升簡樸的解決方案比較有用，他會設一堆係數為 0。讓我用二維來做，

55
00:05:27,840 --> 00:05:37,720
只是給你個幾何式的直覺，我們先從 l2 開始。你必須要用幾何的方式來想，

56
00:05:37,720 --> 00:05:44,190
才能讓它有道理。我要做一個 β1 和 β2 的圖。記得，

57
00:05:44,190 --> 00:05:48,570
正規化讓 β1 和 β2 保持較小，所以正規化要去試著

58
00:05:48,570 --> 00:05:54,990
把這些 β 往這個圖的原點吸。記得正規化項會強迫讓

59
00:05:54,990 --> 00:06:00,530
這個總和很小，所以如果做正規化很大，則 β 會

60
00:06:00,530 --> 00:06:05,200
盡可能的離原點近。現在，假如說我們有足夠的正規化

61
00:06:05,200 --> 00:06:12,100
在我們的最小化的問題裡，正規化項是小的，確切一點假如

62
00:06:12,100 --> 00:06:20,500
我們有足夠的正規化而這個總和最多是 5。也就是說正規化在

63
00:06:20,500 --> 00:06:25,240
試著選擇一個點，落在盡可能最小的圓圈裡，因為 β1 加上

64
00:06:25,240 --> 00:06:32,380
β2 等於 5，就是這個圓圈。但模型也想要是準確的，所以假如說

65
00:06:32,380 --> 00:06:37,430
最準確的模型實際上是在那裡，有這樣的座標。

66
00:06:37,430 --> 00:06:43,790
所以這是非常過適的，因為它只是最小化了訓練錯誤。

67
00:06:43,790 --> 00:06:49,980
然後我放一堆正規化的選擇，所以如果你正規化一點點，

68
00:06:49,980 --> 00:06:55,590
你沒辦法得到那邊那個點，但你可以得到

69
00:06:55,590 --> 00:07:02,690
也許在這裡這個點。所以如果你有太多的正規化，

70
00:07:02,690 --> 00:07:06,960
你會停留在這個小圈裡，而模型也不會很準確，

71
00:07:06,960 --> 00:07:12,040
那樣就會乏適。你會想要一個在中間這裡的，不會乏適也

72
00:07:12,040 --> 00:07:17,260
不會過適，這取決於權衡。所以，當你增加正規化參數，

73
00:07:17,260 --> 00:07:22,150
你的解決方案會從過適開始往下掉，然後他們會

74
00:07:22,150 --> 00:07:27,060
變得很好，然後也許他們會乏適。那麼我們切換來給你

75
00:07:27,060 --> 00:07:33,520
對 l1 範數的幾何直覺，關於 l1 正規化。所以你會有

76
00:07:33,520 --> 00:07:42,169
同樣的東西，取決於不同的權衡。

77
00:07:42,169 --> 00:07:47,010
正規化會把所有的係數往原點吸，它以這種奇怪的方法吸，

78
00:07:47,010 --> 00:07:51,900
整個形狀看起來像個鑽石，如果你畫出 β1 的絕對值加上

79
00:07:51,900 --> 00:07:56,930
β2 的絕對值等於 5，這實際上是一個菱形。

80
00:07:56,930 --> 00:08:05,090
太有趣了。那麼，有最好的訓練錯誤的點，往往但並非總是，

81
00:08:05,090 --> 00:08:11,150
落在其中一個角落這裡。所以這裡的意思就是，

82
00:08:11,150 --> 00:08:17,770
β1 其實是 0。一般在更高的維度中，它更傾向於使用一組係數，

83
00:08:17,770 --> 00:08:23,650
而它們其中很多是 0，所以該模型是簡樸的，且它不具有太多

84
00:08:23,650 --> 00:08:29,490
你需要注意的係數。所以，如果你設置了這樣的問題，

85
00:08:29,490 --> 00:08:36,209
用線性模型和 l1 正規化，這就代表你會得到一個這個問題的解法，

86
00:08:36,208 --> 00:08:41,559
其中有很多的 β 為 0，這樣很好因為那會自動

87
00:08:41,558 --> 00:08:48,480
幫你做特徵選擇。你只需要考慮 β1 和 β6，

88
00:08:48,480 --> 00:08:55,519
和其他任何不為 0 的 β。所以這是我給你看過的兩種正規化項，

89
00:08:55,519 --> 00:09:01,420
這一個 l2，當你添加它的時候也被稱為脊迴歸，

90
00:09:01,420 --> 00:09:07,550
而這個正規化項，l1 項，也被稱為 Lasso 懲罰。

91
00:09:07,550 --> 00:09:13,209
順帶一提，正規化常被稱為收縮，並有一個技術上的原因，

92
00:09:13,209 --> 00:09:17,790
但是當你聽到收縮這個詞，你應該想著正規化，

93
00:09:17,790 --> 00:09:23,069
也就是做一些事讓模型變簡單，或讓它

94
00:09:23,069 --> 00:09:25,339
朝簡單的解決方案收縮。

