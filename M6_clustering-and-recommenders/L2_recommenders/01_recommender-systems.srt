0
00:00:04,970 --> 00:00:09,100
推薦系​​統是一個很大的話題，我們就是沒辦法涵蓋所有的東西，所以我們

1
00:00:09,100 --> 00:00:15,689
要做的是涵蓋特別重要而被廣泛使用的幾個話題，首先我們會聊聊

2
00:00:15,689 --> 00:00:20,880
使用者基礎協同過濾和項目基礎協同過濾。

3
00:00:20,880 --> 00:00:25,310
我不確定你們有多少人聽說過幾年前的 Netflix 比賽，

4
00:00:25,310 --> 00:00:30,199
他們提供了一百萬美金的獎金給第一個能將他們的推薦系統

5
00:00:30,199 --> 00:00:36,750
改進 10％ 的隊伍。而這是他們資料的樣子。對於他們的每個客戶，

6
00:00:36,750 --> 00:00:42,510
對於該客戶看的每部電影都有一個評價，而我們的目標是用群眾的智慧

7
00:00:42,510 --> 00:00:49,930
來試圖找出一個人對於一些他們沒看過的電影會給多少分的評價。

8
00:00:49,930 --> 00:00:57,710
有幾種不同的方法可以做到這一點。而第一個選項是

9
00:00:57,710 --> 00:01:03,660
使用者基礎協同過濾，所以在這裡我們要填寫欠缺的得分，

10
00:01:03,660 --> 00:01:10,120
我們要填寫 Carmen 對於 Alien 的評分，利用類似的使用者的評分。那麼

11
00:01:10,120 --> 00:01:18,580
誰類似 Carmen？可能是 Joseph，以評分來看絕對不會是 Leonore，

12
00:01:18,580 --> 00:01:23,890
所以這是一種方法。而另一種方式就是做項目基礎協同過濾，

13
00:01:23,890 --> 00:01:28,490
比如說我們試圖填補 Joseph 對於 Dark Knight 的評分，

14
00:01:28,490 --> 00:01:35,170
而我們想利用類似的項目評分。那麼，哪些項目類似於 Dark Knight？

15
00:01:35,170 --> 00:01:44,409
那 Cars 絕對是，評分都非常相似，也許 Alien。所以無論是

16
00:01:44,409 --> 00:01:49,430
使用者基礎協同過濾和項目基礎協同過濾兩個都使用了類似的演算法概念，

17
00:01:49,430 --> 00:01:55,880
也就是找到類似的情況，並利用這些來做出推薦。讓我們回到

18
00:01:55,880 --> 00:02:00,970
使用者基礎協同過濾一下，談談如何做這件事。

19
00:02:00,970 --> 00:02:11,120
那麼為了做到，我們需要一種方法來找到類似的用戶。

20
00:02:11,120 --> 00:02:16,880
對於用戶之間的相似性度量，我們其實就直接用相關性。這就只是一般的

21
00:02:16,880 --> 00:02:23,910
Carmen 和使用者 U 之間的相關性。如果 Carmen 評價很高的電影和

22
00:02:23,910 --> 00:02:29,050
使用者 U 高度評價的電影一樣，這將導致較高的相關性。如果 Carmen

23
00:02:29,050 --> 00:02:33,190
評價較低的電影和使用者 U 評價較低的電影一樣，那也會導致

24
00:02:33,190 --> 00:02:37,500
高相關性。但如果他們評價很高的電影是不一樣的，那相關性

25
00:02:37,500 --> 00:02:43,200
就會是較低的。而現在，因為我們在試圖找出哪些電影是 Carmen 會喜歡的，

26
00:02:43,200 --> 00:02:48,910
我們會選擇參考與 Carmen 相關性較高的使用者。

27
00:02:48,910 --> 00:02:53,239
所以現在我們必須想出一個辦法，來決定我們要注意哪些用戶，

28
00:02:53,239 --> 00:02:58,050
然後取平均評分，用 Carmen 的相關性加權。

29
00:02:58,050 --> 00:03:02,610
我要把這個拆開來，然後晚一點就會把它組回來。

30
00:03:02,610 --> 00:03:09,530
現在我想估計的是 Carmen 對於 Alien 的評價，也就是她沒看過的。我們會

31
00:03:09,530 --> 00:03:14,849
從 Carmen 的平均評分開始，然後如果我們覺得她會喜歡那部電影，

32
00:03:14,849 --> 00:03:19,050
則 Alien 的預估評價會比她的平均評價還高，如果我們不覺得她會喜歡，

33
00:03:19,050 --> 00:03:24,349
則預估評價會低於平均。而我們會看所有其他的使用者，

34
00:03:24,349 --> 00:03:31,120
並看看他們是否把 Alien 評價為他們的平均之上。所以在這裡

35
00:03:31,120 --> 00:03:39,280
我們看的是使用者 U 多喜歡 Alien。那麼記得，我們想用每個使用者和 Carmen

36
00:03:39,280 --> 00:03:46,950
之間的相關性來加權他們。因此我們要採取其他用戶意見的加權平均。

37
00:03:46,950 --> 00:03:52,840
因此，我們用他們和 Carmen 之間的相似度來權重他們。

38
00:03:52,840 --> 00:03:57,700
而比其他使用者更相似的會得到較大的權重。然後，因為我們有

39
00:03:57,700 --> 00:04:03,659
這個分母，所以所有的權重加起來會是 1。而這就是 Carmen 對 Alien 的評價

40
00:04:03,659 --> 00:04:10,209
的最終預估值。而這就是使用者基礎協同過濾背後的基本概念。

41
00:04:10,209 --> 00:04:18,930
現在，我要提醒你的一件事是，這個所有電影的總和，就只是

42
00:04:18,930 --> 00:04:23,770
兩個人都評價過的電影。而這個度量有一個問題就是，它往往

43
00:04:23,770 --> 00:04:29,240
偏好一些看的電影很少和 Carmen 重疊，但又對於他們都看過的電影

44
00:04:29,240 --> 00:04:32,720
的評價較一致的使用者。所以這是一件真的很容易出錯的事。

45
00:04:32,720 --> 00:04:38,570
這邊總結一下，這種方法的好處是，它真的很容易，

46
00:04:38,570 --> 00:04:44,770
超級簡單，可能運作得很好。缺點，

47
00:04:44,770 --> 00:04:48,210
如果有很多用戶和電影，而沒有很多用戶評價各部電影的話，

48
00:04:48,210 --> 00:04:51,870
則相似度不會很準確，而估計值也會很差，因為你只要

49
00:04:51,870 --> 00:04:57,150
去看與 Carmen 重疊一點點電影的人，而所有事情就會

50
00:04:57,150 --> 00:05:03,410
變得有點糟糕。還有，使用者基礎協同過濾並不會考慮

51
00:05:03,410 --> 00:05:08,380
項目的相似程度，它只考慮到了用戶的相似程度。

52
00:05:08,380 --> 00:05:12,840
而這就是為什麼我們也會討論項目基礎協同過濾。

53
00:05:12,840 --> 00:05:18,190
對於項目基礎協同過濾，這裡的例子中，我們要填補 Joseph 對於

54
00:05:18,190 --> 00:05:25,650
Dark Knight 的評分，利用類似項目的評價。一樣的，你可以

55
00:05:25,650 --> 00:05:34,940
使用相關性，來用項目和其他項目的相似度權重前面的評價。

56
00:05:34,940 --> 00:05:39,690
在這個例子中，Cars 和 Dark Knight 是非常相似的，Alien 有一點，Bug's Life

57
00:05:39,690 --> 00:05:45,930
則沒那麼相似。而這個的公式實際上和使用者基礎協同過濾

58
00:05:45,930 --> 00:05:49,710
的概念一模一樣。所以我就不重複公式了，因為它是

59
00:05:49,710 --> 00:05:54,690
一模一樣的。總結一下，一樣的，這個方法的好處是簡單。

60
00:05:54,690 --> 00:05:59,990
我要提一下，在現實中，項目基礎協同過濾是比較可靠的，

61
00:05:59,990 --> 00:06:06,639
因為項目與項目之間的相似度傾向於比用戶與用戶之間的相似性更扎實。

62
00:06:06,639 --> 00:06:10,650
但是你也會有和使用者基礎協同過濾一樣的缺點，

63
00:06:10,650 --> 00:06:16,070
因為你有可能對不受歡迎的項目會產生很差的估計。一樣的，這個並不會

64
00:06:16,070 --> 00:06:21,699
考慮到用戶之間的相似程度。那麼在接下來的課堂中，

65
00:06:21,699 --> 00:06:26,759
我要談論矩陣分解，它能同時處理用戶和項目的

66
00:06:26,759 --> 00:06:27,249
相似性。

